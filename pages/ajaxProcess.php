<?php 
include_once("../functions.php"); 
include_once("../config.php"); 

date_default_timezone_set('Europe/Istanbul'); 
$datetime = date("Y-m-d H:i:s"); 
$id = 0;
$resultHTML='';
if($_POST["modalActionType"]=="Add")
{
    $insertResult = add_task($_POST["modalTaskTitle"] , $_POST["modalTaskowner"] , $_POST["modalTaskStatus"]);
    $id=$insertResult;
    if ($insertResult>0)
    {
        $resultHTML=createNewContent($_POST["modalTaskTitle"] , $_POST["modalTaskowner"] , $_POST["modalTaskStatus"],$id,$datetime);
    }
    else
    {
        $resultHTML="The record was not inserted into database.";
    }
}
else 
{
    $editResult = edit_task($_POST["modalTaskTitle"] , $_POST["modalTaskowner"] , $_POST["modalTaskStatus"],$_POST["modalTaskId"]);
    $id=$_POST["modalTaskId"];
    if ($editResult==true)
    {
        $resultHTML=createNewContent($_POST["modalTaskTitle"] , $_POST["modalTaskowner"] , $_POST["modalTaskStatus"],$id,$datetime);
    }
    else
    {
        $resultHTML="The record was not updated in database.";
    }
}

echo $resultHTML;
