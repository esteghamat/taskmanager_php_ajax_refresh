<?php

function delete_task(int $task_id)
{
    global $conn;
    $sql='';
    $sql = 'delete from tasks where id='.$task_id;
    $result = $conn->query($sql);
}

function add_task(string $title , string $owner , string $status)
{
    global $conn;
    $sql = "INSERT INTO  tasks (title , owner , status) VALUES(\"$title\" , \"$owner\" , \"$status\")";
    $insertResult = $conn->query($sql);
    if($insertResult)
    {
        $last_id = $conn->insert_id;
        return $last_id;
    }
    return -1;
}

function edit_task(string $title , string $owner , string $status, int $id)
{
    global $conn;
    $sql = "UPDATE  tasks set title=\"$title\" , owner=\"$owner\" , status=\"$status\" where id=$id ";
    $editResult = $conn->query($sql);
    if($editResult)
    {
        return true;
    }
    return false;
}

function get_tasks(string $where)
{
    global $conn;
    $sql = 'SELECT * FROM '.Task_Table.$where;
    $result = $conn->query($sql);
    $tasks = $result->fetch_all(MYSQLI_ASSOC);
    return $tasks;
}

function call_current_page()
{
    $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
    $currentPage = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
    header("location: $currentPage");
}

function enumDropdown($table_name, $column_name, $echo = false,$elemantId)
{
   global $conn;
   $selectDropdown = "<select id=\"$elemantId\" name=\"modalTask$column_name\">";
   $sql = 'SHOW COLUMNS FROM '.$table_name.' WHERE field="'.$column_name.'"';
   $row = $conn->query($sql)->fetch_all();
    $enumList = explode(",", str_replace("'", "", substr($row[0][1], 5, (strlen($row[0][1])-6))));
    foreach($enumList as $value)
         $selectDropdown .= "<option value=\"$value\">$value</option>";
    $selectDropdown .= "</select>";
    if ($echo)
        echo $selectDropdown;
    return $selectDropdown;
}

function createNewContent($modalTaskTitle , $modalTaskowner , $modalTaskStatus , $id, $datetime)
{
    $task_statuses_var = [1=>'Draft',2=>'Todo' , 3=>'Done' , 4=>'Archive'];
    $htmlResult = '';
    $htmlResult .= '<li class="checked" id="taskrecord_'.$id.'">';
    $htmlResult .= '<i class="fa fa-square"></i>';
    $htmlResult .= '<span class="taskTitle">'.$modalTaskTitle.'</span>';
    $htmlResult .= '<div class="info">';
    $htmlResult .= '<span class="taskOwner">'.$modalTaskowner.'</span>';
    $htmlResult .= '<i name = "editTaskIcon" data-taskrecord="taskrecord_'.$id.'"  data-taskid="'.$id.'" class="fa fa-pencil-square-o editTaskIcon" aria-hidden="true" style="color:darkslateblue"></i>';
    $htmlResult .= '<a href="?del='.$id.'" style="color:red"><i class="fa fa-trash-o"></i></a>';
    $htmlResult .= '<div class="button green taskStatus" data-status="'.(int)$modalTaskStatus.'" >'.$task_statuses_var[(int)$modalTaskStatus].'</div>';
    $htmlResult .= '<span style="font-size: 0.75rem;">Created at : '.$datetime.'</span>';
    $htmlResult .= '</div>';
    return $htmlResult;
}